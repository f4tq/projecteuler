#!/usr/bin/env ruby
=begin
What: Problem1 from http://projecteuler.net/problem=1
Why: I thought it was the hardest problem
Who: Chris Fortescue
Strategy: Create a minimum range to use for iteration.  Pre-calc as much as possible
Copyright: Chris Fortescue, Nov 2013  All rights reserved
Permission hereby granted to use for any reason
=end

start_time = Time.now
debug=false
# just to be a little generalized
the_top=1000
# since the problem says 3 or 5 but not both the closest
# common denominator of both is the product of both: 15
naturals=[3,5,15]
# the min number of naturals dividing the top end will yield the max times we need to iterate. 
rval = (the_top / naturals.min) + 1
# calculate the individual maxes here so they aren't done repeatedly
# doing math repeatedly
max3 = (the_top % 3) == 0 ?  the_top/3 - 1 : the_top/3 
max5 = (the_top % 5) == 0 ?  the_top/5 - 1 : the_top/5
max15 = (the_top % 15) == 0 ? the_top/15 -1 : the_top/15

# save the results of all three and sum them at the end
puts "max3: #{max3} max5: #{max5} max15: #{max15} rval: #{rval}"  if debug
# results[0] == sum 3 accumulator
# results[1] == sum 5 accumulator
# results[2] == sum 15 accumulator
results = [0,0,0]
# only iterate once for speed
# start at one because 0 is arguable not natural and besides, it causes an extra iteration
(1..rval).each do |t| 
  results[0] += t*3 if t <= max3
  results[1] += t*5 if t <= max5 
  if (t <= max15)
    jj = t*15
    # only add to 15's accumulator if number is target number is divisible by both 3 and 5 
    # since the problem states 3 OR 5
    results[2] += jj  if (jj % 3) == 0 && (jj % 5) == 0
  end
end
end_time = Time.now
puts "Results: For 3: #{results[0]} For 5: #{results[1]} Collisions: #{results[2]}" if debug
puts "Answer: #{results[0] + results[1] - results[2]} took #{end_time-start_time} seconds to complete" 

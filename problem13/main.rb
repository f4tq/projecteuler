#!/usr/bin/env ruby
=begin
What: Problem13 from http://projecteuler.net/problem=13
Who: Chris Fortescue (f4tq@yahoo.com)
Why Ruby:  Ruby loves this kind of problem
Strategy: Split the giant into pieces based on powers of 10.  Arbitrarily, I split it 5 times.  Each lines was read into an array then split by 5.  Working
from the least significant to the most significant, the result of dividing by 10**10 (10 raised to the 10th power) was significant to the the next most significant
calculation.

The Answer: 5537376230 took 0.028 seconds to calculate

Copyright:  Chris Fortescue, Nov 2013.  All rights reserved
Permission hereby granted to use for any reason
=end


start_time = Time.now
debug=false
# slurp the file data.txt.
f = File.open('data.txt','r').read
# convert it to array sans newlines
f.gsub!(/\r\n?/,"\n")
ar = f.split(/\n+/)
# split each array element into groups of 10 numbers.  So we have a 
# array of arrays 100x5 
bits = ar.collect{|t| t.scan(/\d{10}/) }
results = Array.new(5)
# make a range comprised of least significant to most significant
myrange=(4..0)

carry_over = 0
# pre-calc the divisor
ten_to_tenth=10**10

finalRez=0

# iterate the range passing the offset number of the range
(myrange.first).downto(myrange.last).each do |offset|
    # use jj to accumulate the sum for all 100 elements
    jj=0
    bits.each do |ele|
      jj += ele[offset].to_i
    end
    puts "jj: #{jj} offset: #{offset} " if debug
    # add the carry_over from the previous calc
    jj += carry_over
    # only carry forward significant digits
    carry_over = jj / ten_to_tenth
    # save the remainder to check math
    results[offset] = jj % ten_to_tenth
    # save the full result of the most significant piece we're after
    finalRez = jj if offset == myrange.last
end 
end_time = Time.now
puts "What: Problem13 http://projecteuler.net/problem=13\nAnswer: #{finalRez.to_s[0..9]} took #{end_time-start_time} seconds to calculate "

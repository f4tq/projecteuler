#!/usr/bin/env node
###

What: The answer to problem59 here http://projecteuler.net/problem=59
Why : Because I love encryption oriented problems.  I gave you, Adobe, source code that demonstrates this
 here: http://tinyurl.com/f4tq4u-1  ruby->pbkdf2
Who : Chris Fortescue (f4tq@yahoo.com)
When: Nov 5, 2013

Overview:
-Reads the cipher, splits it on commas and converts it to an source array.
-Generate all lower (a-z) 3 letter permutations and brut force the key against the cipher
-Short circuits for speed.

Takes ~2 seconds on Macbook Pro

Thanks for 

+ just to make sure on the utf8 <--> ascii8 question http://en.wikipedia.org/wiki/UTF-8

###

start_time = new Date()
debug=false

require('fs')

# I  had to write a permutation class since ruby had a built in function that [coffee|java] didn't
# Basically this class takes a string, splits it into a array then visits every element of the split array.
# Then recursively tries combinations all the while aware of the passed in size.
# @results accumulates valid permutations
# @used is used to keep from infinitely recursing on already scene combos
class Permutate 
  constructor: () ->
       @results = []
       @used=[]
       @output=[]

  getPermutations: (input, size) ->
            chars = input.split('')
            output = []
            @used = new Array(chars.length)
            @output = []
            this.doPermute(chars, size, 0)
            return @results

  doPermute: (input, size, level) ->
    if size == level
      word = @output.join("")
      @results.push word
      return
    level++
    i = 0

    while i < input.length
      if @used[i] is true
          i++
          continue  
      @used[i] = true
      @output.push input[i]
      this.doPermute input, size, level
      @used[i] = false
      @output.pop()
      i++

perm_clf = new Permutate()
#
# keep to the ruby variables names since I did that first 
# and also so you can look side by side at ruby -vs- coffeescript
#
fs = require('fs')
indata = fs.readFileSync 'cipher1.txt','utf8'
indata = indata.replace(/\r\n/g,'\n')
indata = indata.replace(/\n/g,'')
#
# split on comma then convert from string to int
#
rv = (parseInt(val) for val in indata.split(','))

# sanity 
console.log "read #{rv.count} bytes" if debug
puts "read in: #{rv}" if debug

#
# the problem sez lower case implying letters. so start with that
#
letters = 'abcdefghijklmnopqrstuvwxyz'
#
# valid characters culled from http://www.asciitable.com/
# so, I'm considering \t\r\n space-~ ok but chucking DEL/127 for now
#
good_chars = [9,10,13 ].concat [ 32..126]
#
# random words that pop into my head as very common
#
dictionary = ["the","THE","BECAUSE","because","when","was","his","her", "I ","everything","there" ]
#
# holds the final checksum
#
sumit=0
#
# While ruby has a nice permutations function javascript doesn't 
#

# a litte more verbose here than in ruby
all_permutations = perm_clf.getPermutations(letters,3)
ii = 0
key = ''
answerText=''
sumit=0
while ii < all_permutations.length
#
#  convert the chars of the current permutation to its ascii value
#
   combo_bytes = ( v.charCodeAt(0) for v in all_permutations[ii].split(''))

   out = []
   jj=0
# 
#  Now iterate of the ciphered text and xor it against our key bytes
#
   while jj < rv.length
      # need the index so we can figure out which letter from the 3-letter permutation to use
      t = rv[jj]
      #
      # the actual xor 
      #
      v=combo_bytes[jj % 3] ^ t
      #
      # shortcircuit if we see a character that isn't 'text'.  This was harder to do in ruby. And slower
      #
      break if good_chars.indexOf(v) == -1
      # keep accumulating
      out.push v
      jj++
   # 
   # another short circuit that carries percolates the bad_char finding
   #
   if jj < rv.length
      ii++
      continue
#   console.log("(#{ii}) output: #{out} jj: #{jj} rv.length: #{rv.length} ") if all_permutations[ii] == 'god'
   text= out.map  (ord_val) -> String.fromCharCode(ord_val)
   text=text.join('')
#   console.log("(#{ii}) output: #{text} jj: #{jj} rv.length: #{rv.length} ") if all_permutations[ii] == 'god'
   matches=[]
   matches=(word for word in dictionary when text.indexOf(word) != -1)
   if matches.length >= 3
     key = all_permutations[ii]
     answerText = text
     sumit += char_val.charCodeAt(0) for char_val in answerText.split('')
     break;
   ii++
#
# get end time
#
end_date=new Date()
one_day = 86400000
startT = start_time.getTime()/1000.0
end_time = end_date.getTime()/1000.0


console.log "Answer: #{sumit}\nCode: #{key} \nDecoded Text: #{answerText}\nTook: #{end_time - startT} seconds to find"

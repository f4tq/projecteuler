#!/usr/bin/env ruby
=begin

What: The answer to problem59 here http://projecteuler.net/problem=59
Why : Because I love encryption oriented problems.  I gave you, Adobe, source code that demonstrates this
 here: http://tinyurl.com/f4tq4u-1  ruby->pbkdf2
Who : Chris Fortescue (f4tq@yahoo.com)
When: Nov 5, 2013

Overview:
-Reads the cipher, splits it on commas and converts it to an source array.
-Generate all lower (a-z) 3 letter permutations and brut force the key against the cipher
-Short circuits for speed.

Takes ~2 seconds on Macbook Pro



=end

start_time = Time.now
debug=false
# per ruby docs, opening with 'rb' sets the file ascii-8bit.  'r' is UTF-8
# as the cipher was comma delimited so clean it up and convert the string based integers into real ones
rv = File.open('cipher1.txt','rb').read.split(',').map{|t| t.to_i}
# sanity 
puts "read #{rv.count} bytes" if debug
puts "read in: #{rv}" if debug

# the problem sez lower case implying letters. so start with that
letters = ('a'..'z').to_a
# valid characters culled from http://www.asciitable.com/
# so, I'm considering \t\r\n space-~ ok but chucking DEL/127 for now
good_chars = [9,10,13] | (32..126).to_a
# random words that pop into my head as very common
dictionary = ["the","THE","BECAUSE","because","when","was","his","her", "I ","everything","there" ]
# want the short circuit as speed is of the essense
found_it=false
# ruby has a nice permutations function that I have used in searching before to form a search for postgres's ts_vector column type
# now generate all the permutations of 3 letter combos 15558 combinations
found_text=''
sumit=0
keys = letters.permutation(3).select.with_index do |combo,kk|
  # short circuit ruby here because we can't do break in this block
  if !found_it
   # get the ascii code for password via bytes
   combo_bytes = combo.flatten.collect{|t| t.ord}
   # another short circuit to avoid looking at invalid (non-text) characters.  I use that as an indicator that its time to MOVE ON
   bad_char=false
   # I save the characters that I was considering bad
   bady=[]
   # spin over the input cipher1.txt and xor it against the key as appropriate to instructions
   out = rv.collect.with_index do |t,ii|
      # need the index so we can figure out which letter from the 3-letter permutation to use
      v=combo_bytes[ii % 3] ^ t
      # check the resulting key against my bad list
      if !good_chars.include?(v)
        # mark it bad and save the char so i can look at it
        bad_char=true 
        bady.push(v)
      end
      v 
   end
   if !bad_char
     # if we're here, then the ciphered text decoded into something semi sane so
     # now press the resulting text against my dictionary words
     # convert from ord back into readable chars
     out2 = out.collect{|t| t.chr}.flatten.join('')
     # count how many matches occured
     matches = dictionary.select{|t| !out2.index(t).nil?}
     retval=false
     # consider more than 3 a match and cause short-circuit for performance
     if matches.count > 3
       found_it = retval = true
       found_text = out2 
       out.each{|t| sumit += t.ord}
     end
     puts "out(#{kk}) combo: #{combo} found_it: #{found_it} bad_char: #{bad_char} bady: #{bady} #{out2}"  if debug 
     retval
   else
     false
   end
  else
    false
  end
end
end_time = Time.now

puts "Answer: #{sumit}\nCode: #{keys.flatten.join('')} \nDecoded Text: #{found_text}\nTook: #{end_time - start_time} seconds to find"
